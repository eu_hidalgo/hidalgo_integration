Role Name
=========

An Ansible role that creates the off-line mirror for [`spack`](https://spack.readthedocs.io) package manager.
This role has been specifically developed to be used in the HiDALGO project.

Requirements
------------

Current version requires `spack` dependencies such as `bash` and `Python`.

Role Variables
--------------

TODO: A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

TODO: A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Example of how to use the role:

    - name: Prapare local mirror for distributing Spack
      run_once: True
      ansible.builtin.include_role:
        name: create_spack_mirror
        apply:
          delegate_to: 'localhost'
          delegate_facts: True
      vars:
        spack_tarball: '{{ spack_local_tarball.dest }}'

License
-------

BSD 3-close

Author Information
------------------

@copyright (C) 2021
   All rights reserved.

@author Sergiy Gogolenko <gogolenko@hlrs.de>

Developed in the frame of HiDALGO project: https://hidalgo-project.eu/
