Role Name
=========

An Ansible role that installs the [spack](https://spack.readthedocs.io) package manager
in the local directories of the remote hosts (either HPC clusters or testbeds).
This role has been specifically developed to be used in the HiDALGO project.

Description
-----------

We use pilot software environments to create a single mirror with all possible dependent packages
with the predefined depth of mirrored versions at the local workstation.
Afterwards, we copy this mirror to the HPC clusters, as well as install and configure Spack inthere.
The drawback of this approach is that the mirror can be large in side
and include many packages which are already installed at the remote clusters.

![Deployment with a common mirror for all clusters](https://raw.githubusercontent.com/SGo-Go/hidalgo-presentations/main/figs/spack/Spack_deployment_workflow_common_mirror.svg)

Requirements
------------

Current version requires GNU `make` to be available on hosts.

| Dependencies                                             | Local workstation | Target HPC/HPDA system |
|--------------------------------------------------------- |------------------ |----------------------- |
| Python 2.6+ or 3.5+                                      | AS                | S                      |
| Extra modules of Python                                  | A                 |                        |
| `bash`                                                   |                   | AS                     |
| `ssh`                                                    | A                 |                        |
| A C/C++ compiler for building                            |                   | S                      |
| `make`                                                   |                   | S                      |
| `tar`, `gzip`, `unzip`, `bzip2`, `xz`                    | A                 | S                      |
| `patch`                                                  |                   | S                      |
| `git` and `curl` for fetching along with internet access | S                 |                        |

Role Variables
--------------

TODO: A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

TODO: A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Example of how to use the role:

    - hosts: all
      roles:
         - role: spack_deploy
		   spack_archive: ./spack-0.16.1.tar.gz
           spack_configs: ./spack-configs
           spack_mirror:  ./spack-mirror

License
-------

BSD 3-close

Author Information
------------------

@copyright (C) 2021
   All rights reserved.

@author Sergiy Gogolenko <gogolenko@hlrs.de>

Developed in the frame of HiDALGO project: https://hidalgo-project.eu/
