Role Name
=========

An Ansible role that installs the [spack](https://spack.readthedocs.io) package manager
in the local directories of the remote hosts (either HPC clusters or testbeds).
This role has been specifically developed to be used in the HiDALGO project.

Description
-----------

We start by installing and configuring Spack with the pilot software environments on the remote HPC clusters.
Afterwards, we copy `lock`-files of the concertized environments to the local workstation with Internet access
and fetch mirrors separately for each clusters taking into account information about pre-installed software from the =lock=-files.
In the final step, these mirrors are copied to and registered on the corresponding HPC clusters.

![Deployment with mirrors specific for each clusters](https://raw.githubusercontent.com/SGo-Go/hidalgo-presentations/main/figs/spack/Spack_deployment_workflow_specific_mirror.svg)

Requirements
------------

Current version requires GNU `make` to be available on hosts.

| Dependencies                                             | Local workstation | Target HPC/HPDA system |
|--------------------------------------------------------- |------------------ |----------------------- |
| Python 2.6+ or 3.5+                                      | AS                | S                      |
| Extra modules of Python                                  | A                 |                        |
| `bash`                                                   |                   | AS                     |
| `ssh`                                                    | A                 |                        |
| A C/C++ compiler for building                            |                   | S                      |
| `make`                                                   |                   | S                      |
| `tar`, `gzip`, `unzip`, `bzip2`, `xz`                    | A                 | S                      |
| `patch`                                                  |                   | S                      |
| `git` and `curl` for fetching along with internet access | S                 |                        |

Role Variables
--------------

TODO: A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

TODO: A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Example of how to use the role:

    - hosts: all
      tasks:
	- name: Install Spack on clusters
	  ansible.builtin.include_role:
	    name: spack
	  vars:
	    spack_tarball: '{{ spack_local_tarball.dest }}'
	    spack_site_configs: '{{ tempdir_spack_configs.path }}'

License
-------

BSD 3-close

Author Information
------------------

@copyright (C) 2021
   All rights reserved.

@author Sergiy Gogolenko <gogolenko@hlrs.de>

Developed in the frame of the HiDALGO project: https://hidalgo-project.eu/
