#+TITLE: Spack configs
#+AUTHOR: Sergiy Gogolenko
#+EMAIL: gogolenko@hlrs.de

* Introduction and Folder Content

This folder contains two core Ansible scripts =show_hardware.yml= and =sites.yml=.

Script =show_hardware.yml= gathers basic facts about the clusters such as hardware configurations and OS setup.
This is done by calling module =ansible.builtin.setup=.

Script =sites.yml= deploys and configures Spack on the HiDALGO clusters.
At first, it fetches Spack sources from the official GitHub repository
and then deploys and configures it with the files from the folder =spack-configs= (discussed above).
In order to install and configure Spack package manager,
we developed Ansible role =spack=. 
This role installs Spack in the local directory on the HPC clusters
taking into account whether the users have internet access from the cluster or not.
On clusters without internet access,
it automatically creates local off-line mirrors with software packages required by the pilots.
The role supports two models of deployment with local off-line mirrors:
[[https://gitlab.com/eu_hidalgo/hidalgo_integration/-/tree/master/ansible/roles/spack_deploy][deployment with a common mirror for all clusters]] and
[[https://gitlab.com/eu_hidalgo/hidalgo_integration/-/tree/master/ansible/roles/spack][deployment with mirrors specific for each clusters]].

* Installation

- before installing Ansible, make sure that your control node satisfies minimum pre-requisites.
  E.g., if you plan to install Ansible in a virtual environment on Ubuntu machine, run
  #+BEGIN_SRC sh
    sudo apt install python3 python3-venv python3-pip
    python3 -m venv ansible
    . ./ansible/bin/activate
  #+END_SRC
- install Ansible with PyPI directly
  #+BEGIN_SRC sh
    python3 -m pip install paramiko ansible ansible_lint
  #+END_SRC
  or via requirements file
- alternatively you can clone Ansible from =git://github.com/ansible/ansible.git=

** Test the infrastructure

- Change to the ansible folder and store your local credentials to access infrastructure
  #+BEGIN_SRC sh
    mkdir -p ./inventory/group_vars && echo "ansible_user: <your-hlrs-login>" > ./inventory/group_vars/HLRS.yaml
    mkdir -p ./inventory/group_vars && echo "ansible_user: <your-psnc-login>" > ./inventory/group_vars/PSNC.yaml
  #+END_SRC

- check if Ansible uses correct credentials to access HiDALGO HPC infrastructure in =yaml=-format
  #+BEGIN_SRC sh
    ansible-inventory all -i inventory --list -y | less
  #+END_SRC
  or in a human-readable form (option =--graph=)
  #+BEGIN_SRC sh
    ansible-inventory all -i inventory --graph --var
  #+END_SRC
- check accessibility of HiDALGO HPC infrastructure
  #+BEGIN_SRC sh
    ansible all -i inventory -m ping
  #+END_SRC


* Core Playbooks

** Deployment of Spack

Spack can be deployed with the playbook =sites.yaml=
#+BEGIN_SRC sh
  ansible-playbook -i inventory -l hawk,eagle sites.yml
#+END_SRC
If you want to deploy with consise debug information, you can enable tag =never=
#+BEGIN_SRC sh
  ansible-playbook -i ./inventory --tags never,all ./sites.yml -l hawk
#+END_SRC

As long as Spack is deployed, you can login to host and use it. E.g.,
#+BEGIN_SRC sh
  . ~/spack-hidalgo/hawk/share/spack/setup-env.sh 
  spack env activate sna
#+END_SRC

** Collecting facts

In order to show hardware configuration for benchmark reports, one can use the playbook =show_hardware.yml=:
#+BEGIN_SRC sh
  ansible-playbook -i inventory -l hawk,eagle show_hardware.yml
#+END_SRC
To collect hardware information on the compute node, one need to run this playbook locally with option ~--connection=local~:
#+BEGIN_SRC sh
  ansible-playbook all -i 127.0.0.1, -m show_hardware --connection=local
#+END_SRC
Alternatively, one can gather [[https://docs.ansible.com/ansible/latest/collections/ansible/builtin/setup_module.html][facts]] of interest with the standard =setup= module as shown in examples below:
  #+BEGIN_SRC sh
    ansible all -i 127.0.0.1, -m ansible.builtin.setup --connection=local | less
    ansible all -i 127.0.0.1, -m ansible.builtin.setup -a 'gather_subset=!all,!min,hardware filter=*processor*' --connection=local | less
    ansible HLRS -i inventory -m ansible.builtin.setup -a 'gather_subset=!all,!min,hardware filter=*processor* fact_path=hlrs-hardware.json'
  #+END_SRC

* FAQ
** How to improve Ansible Output Format
- in order to improve readability, use =yaml= [[https://docs.ansible.com/ansible/latest/plugins/callback.html][callback plugin]].
  #+BEGIN_SRC sh
    ANSIBLE_LOAD_CALLBACK_PLUGINS=1 ANSIBLE_STDOUT_CALLBACK=yaml ansible ...
    ANSIBLE_STDOUT_CALLBACK=yaml ansible-playbook ...
  #+END_SRC
  or append the following lines to your =~/.ansible.cfg=
  #+BEGIN_SRC yaml
    [defaults]
    # Use the YAML callback plugin.
    stdout_callback = yaml
    # Use the stdout_callback when running ad-hoc commands.
    bin_ansible_callbacks = True
  #+END_SRC
