# -------------------------------------------------------------------------
# This file controls default concretization preferences for Spack
# at Vulcan/cascadelake system.
# -------------------------------------------------------------------------

# include:
# - ./hidalgo/packages.yaml
# packages:
  all:
    compiler: [gcc@10.2.0, gcc, intel, clang, pgi]
    target: [cascadelake]
    providers:
      cmake: [cmake]
      pkgconfig: [pkg-config]
      mpi: [openmpi, intel-mpi, mvapich2]
      tbb: [intel-tbb]
      libflame: [amdlibflame]
      blis: [amdblis]
      mkl: [intel-mkl]
      blas: [intel-mkl, amdblis]
      lapack: [intel-mkl, amdlibflame]
      scalapack: [scalapack, amdscalapack]
      fftw: [fftw, amdfftw]
      jpeg: [libjpeg-turbo, libjpeg]
      zoltan: [trilinos]
      python: [python]
      mpi4py: [py-mpi4py]
      yacc: [bison]
      elf: [libelf]

  # MPI config on Vulcan
  mpi:
    buildable: False

  openmpi:
    # with modulefiles finds the correct paths for different compilers
    externals:
    - spec: openmpi@4.0.5%gcc@10.2.0
      modules:
      - mpi/openmpi/4.0.5-gnu-10.2.0
    - spec: openmpi@4.0.5%gcc@9.2.0
      modules:
      - mpi/openmpi/4.0.5-gnu-9.2.0
    - spec: openmpi@4.0.5%intel@19.1.0
      modules:
      - mpi/openmpi/4.0.5-intel-19.1.0
  mpich:
    buildable: False
  mvapich2:
    externals:
    - spec: mvapich2@2.3.1
      modules:
      - mpi/mvapich2/2.3.1-gnu-8.3.0
  intel-mpi:
    externals:
    - spec: intel-mpi@5.1.3.223
      modules:
      - mpi/impi/5.1
  spectrum-mpi:
    buildable: False
  mpilander:
    buildable: False

  # Optimized HPC libraries provided by vendors
  # @TODO fix MKL concretization
  intel-mkl:
    buildable: False
    externals:
    - spec: intel-mkl@19.0.3
      modules:
      - numlib/intel/mkl/19.0.3
  # intel-mkl:
  #   buildable: False
  #   externals:
  #   - spec: intel-mkl@19.0.3 +fortran ~cxx ~ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 +fortran ~cxx +ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 +fortran ~cxx ~ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 +fortran ~cxx +ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 ~fortran +cxx ~ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 ~fortran +cxx +ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 ~fortran +cxx ~ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3
  #   - spec: intel-mkl@19.0.3 ~fortran +cxx +ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - numlib/intel/mkl/19.0.3

  # Common build time dependencies detected by "spack externals find"
  xz:
    externals:
    - spec: xz@5.2.2
      prefix: /usr
    buildable: false
  texinfo:
    externals:
    - spec: texinfo@5.1
      prefix: /usr
    buildable: false
  tar:
    externals:
    - spec: tar@1.26
      prefix: /usr
    buildable: false
  pkg-config:
    externals:
    - spec: pkg-config@0.27.1
      prefix: /usr
    buildable: false
  perl:
    externals:
    - spec: perl@5.16.3+cpanm+shared+threads
      prefix: /usr
    buildable: false
  openssl:
    externals:
    - spec: openssl@1.0.2k-fips
      prefix: /usr
    buildable: false
  openjdk:
    externals:
    - spec: openjdk@1.8.0_282-b08
      prefix: /usr
    buildable: false
  # ncurses:
  #   externals:
  #   - spec: ncurses@5.9.20130511+termlib
  #     prefix: /usr
  #   buildable: false
  m4:
    externals:
    - spec: m4@1.4.16
      prefix: /usr
    buildable: false
  lustre:
    externals:
    - spec: lustre@2.12.6
      prefix: /usr
    buildable: false
  libtool:
    externals:
    - spec: libtool@2.4.2
      prefix: /usr
    buildable: false
  krb5:
    externals:
    - spec: krb5@1.15.1
      prefix: /usr
    buildable: false
  gmake:
    externals:
    - spec: gmake@3.82
      prefix: /usr
    buildable: false
  git:
    externals:
    - spec: git@1.8.3.1~tcltk
      prefix: /usr
    buildable: false
  ghostscript:
    externals:
    - spec: ghostscript@9.25
      prefix: /usr
    buildable: false
  flex:
    externals:
    - spec: flex@2.5.37+lex
      prefix: /usr
    buildable: false
  findutils:
    externals:
    - spec: findutils@4.5.11
      prefix: /usr
    buildable: false
  diffutils:
    externals:
    - spec: diffutils@3.3
      prefix: /usr
    buildable: false
  cpio:
    externals:
    - spec: cpio@2.11
      prefix: /usr
    buildable: false

  cmake:
    version: [3.17.3, 3.11.1, 2.8.12.2]
    externals:
    - spec: cmake@2.8.12.2
      prefix: /usr
    - spec: cmake@3.11.1
      modules: [tools/cmake/3.11.1]
    - spec: cmake@3.17.3
      modules: [tools/cmake/3.17.3]

    buildable: false
  bzip2:
    externals:
    - spec: bzip2@1.0.6
      prefix: /usr
    buildable: false
  bison:
    externals:
    - spec: bison@3.0.4
      prefix: /usr
    buildable: false
  bash:
    externals:
    - spec: bash@4.2.46
      prefix: /usr
    buildable: false
  automake:
    externals:
    - spec: automake@1.13.4
      prefix: /usr
    buildable: false
  autoconf:
    externals:
    - spec: autoconf@2.69
      prefix: /usr
    buildable: false

  binutils:
    externals:
    - spec: binutils@2.30
      modules:
      - tools/binutils/2.30

  # Interpreters
  ruby:
    buildable: false
    externals:
    - spec: ruby@2.0.0
      prefix: /usr

  # Spack requires PYTHONPATH to be unset, therefore use prefix instead of module
  python:
    buildable: false
    version: [3.6.6, 2.7.15, 2.7.5]
    externals:
    - spec: python@3.6.6
      prefix: /opt/hlrs/python-site/vanilla_python/3.6.6
      # modules:
      # - python/3.6
    - spec: python@2.7.15
      prefix: /opt/hlrs/python-site/vanilla_python/2.7.15
      # modules:
      # - python/2.7
    - spec: python@2.7.5+bz2+ctypes+dbm+lzma+nis+pyexpat+readline+sqlite3+ssl~tix~tkinter+uuid+zlib
      prefix: /usr
    # - spec: python@3.6.8+bz2+ctypes+dbm+lzma+nis+pyexpat+readline+sqlite3+ssl~tix~tkinter+uuid+zlib
    #   prefix: /usr

  # Extensions

  # # Compilation settings of mpi4py can be revealed with:
  # #
  # # $ python -c "import mpi4py; print(mpi4py.get_config())"
  # #
  # py-mpi4py:
  #   buildable: False
  #   externals:
  #   - spec: py-mpi4py ^python@3:
  #     modules:
  #     - python/3.6
  #   - spec: py-mpi4py ^python@2:
  #     modules:
  #     - python/2.7

  cuda:
    externals:
    - spec: cuda@10.0.130
      modules:
      - system/cuda/10.0.130
    - spec: cuda@10.1.243
      modules:
      - system/cuda/10.1.243
    - spec: cuda@9.2.88
      modules:
      - system/cuda/9.2.88
    - spec: cuda@9.1.85
      modules:
      - system/cuda/9.1.85
