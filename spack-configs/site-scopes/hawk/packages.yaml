# -------------------------------------------------------------------------
# This file controls default concretization preferences for Spack
# at Hawk system.
# -------------------------------------------------------------------------
# include:
# - ./hidalgo/packages.yaml
# packages:
  all:
    compiler: [gcc@9.2.0, gcc@10.2.0, gcc, intel, clang]
    target: [zen2]
    providers:
      cmake: [cmake]
      pkgconfig: [pkg-config]
      mpi: [mpt, openmpi]
      tbb: [intel-tbb]
      libflame: [amdlibflame]
      blis: [amdblis]
      mkl: [intel-mkl]
      blas: [intel-mkl, amdblis]
      lapack: [intel-mkl, amdlibflame]
      scalapack: [scalapack, amdscalapack]
      fftw: [fftw, amdfftw]
      jpeg: [libjpeg-turbo, libjpeg]
      zoltan: [trilinos]
      python: [python]
      mpi4py: [py-mpi4py]
      yacc: [bison]
      elf: [libelf]

  # MPI config on Hawk
  mpi:
    buildable: False

  mpt:
    externals:
    - spec: mpt@2.22
      modules:
      - mpt/2.22
    - spec: mpt@2.23
      modules:
      - mpt/2.23

  openmpi:
    # with modulefiles finds the correct paths for different compilers
    externals:
    - spec: openmpi@4.0.5
      modules:
      - openmpi/4.0.5
    - spec: openmpi@4.0.4
      modules:
      - openmpi/4.0.4

  mpich:
    buildable: False
  mvapich2:
    buildable: False
  intel-mpi:
    buildable: False
  intel-parallel-studio:
    buildable: False
  spectrum-mpi:
    buildable: False
  mpilander:
    buildable: False

  charm:
    buildable: False
  charmpp:
    buildable: False

  # Optimized HPC libraries provided by vendors
  # @TODO: Sometimes this record leads to "No headers found for intel-tbb"
  intel-tbb:
    externals:
    - spec: intel-tbb@19.1.0%intel@19.1.0
      modules:
      - tbb/19.1.0
    - spec: intel-tbb@19.1.0%gcc
      modules:
      - tbb/19.1.0

  # @TODO: resolve issue on compiling py-numpy with MKL back-end
  intel-mkl:
    buildable: False
    externals:
    - spec: intel-mkl@19.1.0
      modules:
      - mkl/19.1.0

  # intel-mkl:
  #   buildable: False
  #   # The installed version of MKL contains lp64 _and_ ilp64 as well as
  #   # shared _and_ static variants and hence can satisfy all combinations.
  #   # Unfortunately, Spack considers specs with ~ilp64 or ~shared not to
  #   # be satisfied by the +ilp64 +shared entry below.
  #   # However, specifying all combinations here (in separate lines) is
  #   # not a solution since Spack chooses those with only the features
  #   # _explicitly_ asked for by the client package.
  #   # Hence please adapt client packages to ask for +ilp64 and +shared
  #   # although those features are not actually required.
  #   externals:
  #   - spec: intel-mkl@19.1.0 +fortran ~cxx ~ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 +fortran ~cxx +ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 +fortran ~cxx ~ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 +fortran ~cxx +ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 ~fortran +cxx ~ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 ~fortran +cxx +ilp64 +shared ~static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 ~fortran +cxx ~ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0
  #   - spec: intel-mkl@19.1.0 ~fortran +cxx +ilp64 ~shared +static threads=none,openmp,tbb
  #     modules:
  #     - mkl/19.1.0

  # Common build time dependencies detected by "spack externals find"
  autoconf:
    buildable: False
    externals:
    - spec: autoconf
      prefix: /usr

  automake:
    buildable: False
    externals:
    - spec: automake
      prefix: /usr

  m4:
    buildable: False
    version: [1.4.18]
    externals:
    - spec: m4@1.4.16
      prefix: /usr

  cmake:
    buildable: False
    version: [3.16.4, 3.11.4]
    externals:
    - spec: cmake@3.11.4
      prefix: /usr
    - spec: cmake@3.16.4
      modules:
      - cmake/3.16.4

  # @TODO: some dependencies do not link with this version of binutils
  # binutils:
  #   buildable: False
  #   version: [2.35]
  #   externals:
  #   - spec: binutils@2.30
  #     prefix: /usr
  #   - spec: binutils@2.35+libiberty~nls
  #     modules:
  #     - binutils/2.35

  git:
    buildable: False
    externals:
    - spec: git
      prefix: /usr

  pkg-config:
    buildable: False
    externals:
    - spec: pkg-config
      prefix: /usr

  bison:
    buildable: False
    version: [3.4]
    externals:
    - spec: bison@3.4
      prefix: /usr

  flex:
    buildable: False
    externals:
    - spec: flex
      prefix: /usr

  openssl:
    buildable: False
    externals:
    - spec: openssl
      prefix: /usr

  bzip2:
    buildable: False
    version: [1.0.6]
    externals:
    - spec: bzip2@2.4.2
      prefix: /usr

  tar:
    buildable: False
    externals:
    - spec: tar
      prefix: /usr

  xz:
    buildable: False
    externals:
    - spec: xz
      prefix: /usr

  # Interpreters
  perl:
    buildable: False
    externals:
    - spec: perl
      prefix: /usr

  # @TODO: So far build python from sources
  # python:
  #   buildable: False
  #   version: [3.8.3, 3.6.8, 2.7.16]
  #   externals:
  #   - spec: python@3.6.8
  #     prefix: /usr
  #   - spec: python@2.7.16
  #     prefix: /usr
  #   # @TODO: find out a proper way to tell Spack about libs for python@3.8.3
  #   - spec: python@3.8.3+shared
  #     modules:
  #     - hidalgo/python/3.8

  # # Extensions
  # py-mpi4py:
  #   buildable: False
  #   externals:
  #   - spec: py-mpi4py@3.0.3%gcc@10.2.0 ^mpt
  #     prefix: /opt/hlrs/non-spack/development/python/mpi4py-3.0.3-mpt-2.23-gcc-9.2.0/lib/python3.8/site-packages
  #     # modules:
  #     # - mpi4py/3.0.3

  # Other non-optimized tools
  swig:
    buildable: False
    version: [3.0.12]
    externals:
    - spec: swig@3.0.12
      prefix: /usr

  gettext:
    buildable: False
    externals:
    - spec: gettext
      prefix: /usr

  libtool:
    buildable: False
    version: [2.4.6]
    externals:
    - spec: libtool@2.4.6
      prefix: /usr

  libedit:
    buildable: False
    version: [0.0.46]
    externals:
    - spec: libedit@0.0.46
      prefix: /usr

  zlib:
    buildable: False
    externals:
    - spec: zlib
      prefix: /usr

  readline:
    buildable: False
    externals:
    - spec: readline
      prefix: /usr

  unzip:
    buildable: False
    externals:
    - spec: unzip
      prefix: /usr

  expat:
    buildable: False
    externals:
    - spec: expat~libbsd
      prefix: /usr

  libpng:
    buildable: False
    externals:
    - spec: libpng
      prefix: /usr

  curl:
    buildable: False
    externals:
    - spec: curl
      prefix: /usr

  dbus:
    buildable: False
    externals:
    - spec: dbus
      prefix: /usr

  qt:
    buildable: False
    externals:
    - spec: qt
      prefix: /usr

  numactl:
    buildable: False
    externals:
    - spec: numactl
      prefix: /usr

  # Commented out to enable Python build
  # gdbm:
  #   buildable: False
  #   externals:
  #   - spec: gdbm
  #     prefix: /usr

  libffi:
    buildable: False
    externals:
    - spec: libffi
      prefix: /usr

  libjpeg:
    buildable: False
    externals:
    - spec: libjpeg
      prefix: /usr

  libxaw:
    buildable: False
    externals:
    - spec: libxaw
      prefix: /usr

  gmp:
    buildable: False
    externals:
    - spec: gmp
      prefix: /usr

  glib:
    buildable: False
    externals:
    - spec: glib
      prefix: /usr

  cairo:
    buildable: False
    externals:
    - spec: cairo+pdf
      prefix: /usr

  libtiff:
    buildable: False
    externals:
    - spec: libtiff
      prefix: /usr

  pango:
    buildable: False
    externals:
    - spec: pango
      prefix: /usr

  pcre:
    buildable: False
    externals:
    - spec: pcre
      prefix: /usr

  guile:
    buildable: False
    externals:
    - spec: guile
      prefix: /usr

  papi:
    compiler: [gcc@8.3.1]
    target: [zen]
