#+TITLE: Spack configs
#+AUTHOR: Sergiy Gogolenko
#+EMAIL: gogolenko@hlrs.de

* Introduction and Folder Content

Content of the Spack configs folder follows guidelines of the Extreme-scale Scientific Software Stack ([[https://e4s-project.github.io/][E4S]]) project.
Namely, it includes three subfolders:
- =repos/hidalgo= with the custom Spack packaging scripts,
- =site-scopes= with the configurations for the target HPC/HPCA systems, and
- =stacks= with the use case software stacks.

Subfolder =repos/hidalgo= serves to store Spack packages for HiDALGO
which are either configured unconventionally or are not the part of the builtin Spack package repository yet.
In particular, it contains definition of
package =libosrm=,
an OSRM backend library which is used for routing in location graph extraction codes of the migration pilot.

Subfolder =site-scopes= contains configurations for the clusters Hawk, Vulcan, and Eagle/Altair, 
which constitute the core HPC and HPDA hardware infrastructure of the HiDALGO.
Configurations files for each cluster are stored in a separate subfolder which includes:
- =config.yaml= with generic configuration options,
- =compilers.yaml= with compiler definitions,
- =packages.yaml= with build customization, and 
- =upstreams.yaml= with installation trees of external =Spack= instances (if applicable).
These files are prepared manually and regularly updated by the HiDALGO team
in case of changes at the default configurations and software stacks of the HiDALGO clusters.
Config files =mirrors.yaml= and =repos.yaml= are configured automatically by Ansible scripts as discussed below. 
After configuring,
=repos.yaml= stores link to the copy of the HiDALGO repository =repos/hidalgo= at the target cluster,
while =mirrors.yaml= holds link to the local automatically created off-line mirror for the clusters with Internet ban (Hawk and Vulcan)
and link to the default on-line Spack mirror for the clusters with open Internet access (Eagle).

Subfolder =stacks= contains definitions of software environments for the use cases.

* Deployment of Spack with HiDALGO configs
** Spack on the HiDALGO systems

- Allocate space fot the Spack installation and make a symlink to it
  #+BEGIN_SRC sh
    ws_allocate -d 30 spack && ln -s $(ws_find spack) ~/spack-build
  #+END_SRC
  or 
  #+BEGIN_SRC sh
    mkdir -p ./spack && ln -s ./spack ~/spack-build
  #+END_SRC
- Copy configs, Spack and mirror to the HiDALGO system
  #+BEGIN_SRC sh
    export HIDALGO_SYSTEM=hawk
    scp -r ../spack-configs $HIDALGO_SYSTEM:~
    curl -LJ https://github.com/spack/spack/releases/download/v0.16.1/spack-0.16.1.tar.gz | \
        ssh $HIDALGO_SYSTEM '(tar -zxf - -C ~/spack-build/ && mv ~/spack-build/spack* ~/spack-build/spack)'
    scp -r ./hidalgo_mirror $HIDALGO_SYSTEM:~/spack-build/spack/mirrors/hidalgo
  #+END_SRC
- Patch default Spack distribution with site configs
  #+BEGIN_SRC sh
    cd ~/spack-configs
    SPACK_ROOT=$(readlink -f ~/spack-build)/spack \
    SPACK_HIDALGO_MIRROR=$(readlink -f ~/spack-build)/hidalgo_mirror \
    make install
  #+END_SRC

* Usage 

** Native Spack

- Activate Spack
  #+BEGIN_SRC sh
    . ~/spack-build/spack/share/spack/setup-env.sh
  #+END_SRC
- create environment and work with it as usually
  #+BEGIN_SRC sh
    export HIDALGO_PILOT=sna
    spack env create $HIDALGO_PILOT ~/spack-configs/stacks/$HIDALGO_PILOT/spack.yaml 
    spack env activate $HIDALGO_PILOT
    spack concretize -f
    spack install
  #+END_SRC

** Modulefiles

If you prefer to use ~module load~ instead of ~spack load~,
you should generate modulefiles.
#+BEGIN_SRC sh
  spack module refresh tcl libosrm
#+END_SRC
Afterwards you can declare path to modulefiles and use them
#+BEGIN_SRC sh
  module use ~/spack-build/spack/share/spack/modules/linux-centos8-zen2
  spack module refresh tcl libosrm
#+END_SRC

* Autodetection of compilers and packages

- autodetect compilers
  #+BEGIN_SRC sh
    module load compiler/gnu
    spack compiler find --scope site gcc
    module unload compiler/gnu
  #+END_SRC
- autodetect external build time dependencies
  #+BEGIN_SRC sh
    for pkg in $(spack external list | grep "    " | tr -d " "); do
	echo $pkg; spack external find --not-buildable $pkg;
    done
  #+END_SRC
- versions of Python extensions
  #+BEGIN_SRC sh
    python -c "import mpi4py; print(mpi4py.get_config())"
  #+END_SRC

* Troubleshooting
** Enhanced concretization with [[https://github.com/potassco/clingo][clingo]] at Vulcan (see issue [[https://github.com/spack/spack/issues/19951][#19951]])

  If default concretizer produces errors at Vulcan/Hawk (e.g., like the one from the issue [[https://github.com/spack/spack/issues/19951][#19951]]),
  install [[https://github.com/potassco/clingo][clingo]] and use this enhanced concretizer
  #+BEGIN_SRC sh
    spack install clingo%aocc+python ^python@2.7.5
    spack load clingo
    python -m pip install --user --upgrade clingo
    spack find -p clingo
  #+END_SRC

** Interference of old caches and configs for the user scope
  After installation of new Spack instance, cleaning up for =~/.spack= might be required
  #+BEGIN_SRC sh
    rm -rf ~/.spack/*
  #+END_SRC

** Unset =PYTHONPATH= for correct installation of =Python= packages

  Spack requires =PYTHONPATH= to be unset when install Python packages,
  therefore prefer to use =prefix= instead of =module=
  for defining vanilla Python in =packages.yaml= from site configs like below:
  #+BEGIN_SRC yaml
     python:
       externals:
         - spec: python@3.6.6
           prefix: /opt/hlrs/python-site/vanilla_python/3.6.6
           # modules:
           # - python/3.6
  #+END_SRC

** Manual modulefiles

  Sometimes there is a need to patch default Modulefiles at the system.
  This is a possible way to do it.
  #+BEGIN_SRC sh
    export PACKAGE_NAME=python
    mkdir -p $HOME/.modulefiles/hidalgo
    cp -rL $(find $(echo $MODULEPATH | tr : " ") -name $PACKAGE_NAME) ~/.modulefiles/hidalgo/$PACKAGE_NAME
    # apply your patch or edit modulefile "vi/patch ~/.modulefiles/hidalgo/$PACKAGE_NAME"
    module use $HOME/.modulefiles
  #+END_SRC

** Environments with a single spec

As an alternative to environments, one can consider to use [[https://spack.readthedocs.io/en/latest/build_systems/bundlepackage.html][BundlePackage]]:
#+BEGIN_SRC sh
  spack create --template bundle --name lge
#+END_SRC

** Build breaks: What to do before contacting Sergiy?

- clean up build for failed package
  #+BEGIN_SRC sh
    export HIDALGO_FAILED_SPEC=libosrm%gcc@9.2.0 ^intel-tbb@2020.3
    spack clean $HIDALGO_FAILED_SPEC
  #+END_SRC
- [[https://spack.readthedocs.io/en/latest/packaging_guide.html#spack-build-env][enter build environment]] to find out problem
  #+BEGIN_SRC sh
    spack stage $HIDALGO_FAILED_SPEC
    spack cd $HIDALGO_FAILED_SPEC
    spack build-env $HIDALGO_FAILED_SPEC bash
  #+END_SRC
- try to configure and build package by hand
  #+BEGIN_SRC sh
    spack cd --build-dir $HIDALGO_FAILED_SPEC
    mkdir ./test && cd ./test
    rm -f CMakeCache.txt && LDFLAGS="-L/lib64 -lpthread" cmake .. && make
  #+END_SRC
- try fast rebuild and repeat the process if change didn't help
  #+BEGIN_SRC sh
    spack install --fail-fast libosrm
  #+END_SRC

* Remarks

This work is inspired by [[https://github.com/E4S-Project][E4S]] project.
