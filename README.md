# HiDALGO integration scripts

Integration scripts for HiDALGO software including Spack packages and configs,
as well as GitLab-CI scripts for use cases and individual tools.

## Automated Deployment

HiDALGO uses combination of Ansible and Spack as a final solution for automated deployment.
Compared to alternatives discussed in previous deliverables (like containerization),
this approach allowed to achieve the following goals:

-   support combinatorial versioning and reproducible software environments for all use cases over HiDALGO platforms and testbeds;
-   customize builds consistently for each platform;
-   facilitate separable software environments which reuse available software installations;
-   support installation matrices for benchmarks and performance studies (see D5.8);
-   enable off-line bare metal installation along with a full control on the installation process.

Spack is a HPC-oriented package manager for installing software packages from sources
which allows to optimize the software to the target architectures and study effects of different configurations.
Besides support for the management of dependencies, it resolves classic issues, typical for installation of software in HPC environments,
such as combinatorial versioning, ban for internet access at some sites,
accounting for site- and system-specific details (diversity of build systems, compilers, recommended setups, pre-installed software stacks).
In addition, Spack supports separable software environments,
which can be automatically converted to containerization recipes for Docker and Singularity.
In order to deploy and configure Spack at the target systems, we use Ansible.
Both Ansible and Spack feature deep integration of YAML and JSON formats along with a high extensibility
via a wide range of builtin and external modules.
It makes these tools a perfect combination not only for automated fully-reproducible software installation,
but also for documenting hardware and software configurations along with the installation process
uniformly in a human readable format, easy to post-process.
Last, but not least, automated deployment approach with Ansible and Spack depends on the software
which is naively installed on the most of existing HPC and HPDA platforms,
and requires Internet access only on the workstation with Ansible manager.

| Dependencies                                             | Local workstation | Target HPC/HPDA system |
|--------------------------------------------------------- |------------------ |----------------------- |
| Python 2.6+ or 3.5+                                      | AS                | S                      |
| Extra modules of Python                                  | A                 |                        |
| `bash`                                                   |                   | AS                     |
| `ssh`                                                    | A                 |                        |
| A C/C++ compiler for building                            |                   | S                      |
| `make`                                                   |                   | S                      |
| `tar`, `gzip`, `unzip`, `bzip2`, `xz`                    | A                 | S                      |
| `patch`                                                  |                   | S                      |
| `git` and `curl` for fetching along with internet access | S                 |                        |

This repository includes two AD/SCM-related folders:

-   `ansible` which contains Ansible scripts for deploying and configuring Spack on the HiDALGO HPC/HPDA infrastructure, and
-   `spack-configs` which contains configuration files of Spack.

Both folders are supplemented with the detailed `README.org` files.
